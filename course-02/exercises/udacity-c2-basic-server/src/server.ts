import express, { Router, Request, Response } from 'express';
import bodyParser from 'body-parser';

import { Car, cars as cars_list } from './cars';
import { request } from 'http';

(async () => {
  let cars:Car[] = cars_list;

  //Create an express applicaiton
  const app = express(); 
  //default port to listen
  const port = 8082; 
  
  //use middleware so post bodies 
  //are accessable as req.body.{{variable}}
  app.use(bodyParser.json()); 

  // Root URI call
  app.get( "/", ( req: Request, res: Response ) => {
    res.status(200).send("Welcome to the Cloud!");
  } );

  // Get a greeting to a specific person 
  // to demonstrate routing parameters
  // > try it {{host}}/persons/:the_name
  app.get( "/persons/:name", 
    ( req: Request, res: Response ) => {

      if ( !req.params.name ) {
        return res.status(400)
                  .send(`name is required`);
      }

      return res.status(200)
                .send(`Welcome to the Cloud, ${req.params.name}!`);
  } );

  // Get a greeting to a specific person to demonstrate req.query
  // > try it {{host}}/persons?name=the_name
  app.get( "/persons/", ( req: Request, res: Response ) => {
    let { name } = req.query;

    if ( !name ) {
      return res.status(400)
                .send(`name is required`);
    }

    return res.status(200)
              .send(`Welcome to the Cloud, ${name}!`);
  } );

  // Post a greeting to a specific person
  // to demonstrate req.body
  // > try it by posting {"name": "the_name" } as 
  // an application/json body to {{host}}/persons
  app.post( "/persons", 
    async ( req: Request, res: Response ) => {

      if ( !req.body.name ) {
        return res.status(400).send(`name is required`);
      }

      return res.status(200).send(`Welcome to the Cloud, ${req.body.name}!`);
  } );

  // @TODO Add an endpoint to GET a list of cars
  // it should be filterable by make with a query paramater

  app.get('/cars', (req : Request, res : Response) => {

    var found = true;
    var found_one = false;
    var found_cars:Car[] = [];

    cars.forEach(elem => {
        if (req.query.make && req.query.make !== elem.make)
          {
            found = false;
          }
          

        if (req.query.type && req.query.type !== elem.type)
          found = false;

        if (req.query.model && req.query.model !== elem.model)
          found = false;

        if (parseInt(req.query.cost) && parseInt(req.query.cost) !== elem.cost)
          found = false;

        if (parseInt(req.query.id) && parseInt(req.query.id) !== elem.id)
          found = false;


        if (found === true) {
          found_cars.push(elem);
        }
        else {
          found = true;
        }

    });

    

    if(found_cars.length === 0 && ! (req.query.mark || req.query.type || req.query.model || req.query.cost || req.query.id))
      return res.status(200).send(JSON.stringify({
        'cars': cars,
      }));
    else if (found_cars.length !== 0) {
      res.status(200).send(JSON.stringify(found_cars));
    }
    else {
      res.status(404).send("Resource was not found :(");
    }
      

  })

  app.get('/cars/:id', (req : Request, res : Response) => {

    var found = false;
    cars.forEach(elem => {
      if (elem.id === parseInt(req.params.id)) {
        res.status(200).send(JSON.stringify(elem));
        found = true;
      }
    });

    if(!found)
      res.status(404).send("Invalid ID ! Resource was not found :(");
    
  })

  app.post('/cars', (req : Request, res : Response) => {

    let car:Car = { make: req.body.make, type: req.body.type, model: req.body.model, cost: parseInt(req.body.cost), id: parseInt(req.body.id) };

    cars = [...cars, car]
    
    res.status(200).send(JSON.stringify(car));

  })

  // Start the Server
  app.listen( port, () => {
      console.log( `server running http://localhost:${ port }` );
      console.log( `press CTRL+C to stop server` );
  } );
})();